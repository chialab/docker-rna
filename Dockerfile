FROM ubuntu:16.04

RUN apt-get update && apt-get install -y git

# Install node
COPY ./setup_node.sh /tmp
RUN bash /tmp/setup_node.sh
RUN rm -rf /tmp/setup_node.sh
RUN apt-get update && apt-get install -y nodejs

# Install RNA CLI
RUN npm install -g git+https://gitlab.com/chialab/rna-cli